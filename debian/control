Source: fonts-creep
Section: fonts
Priority: optional
Maintainer: Debian Fonts Team <debian-fonts@lists.debian.org>
Uploaders:
 Agathe Porte <debian@microjoe.org>,
Build-Depends:
 debhelper-compat (= 13),
 fontmake,
Standards-Version: 4.6.0.1
Homepage: https://github.com/romeovs/creep
Vcs-Git: https://salsa.debian.org/fonts-team/fonts-creep.git
Vcs-Browser: https://salsa.debian.org/fonts-team/fonts-creep
Rules-Requires-Root: no

Package: fonts-creep
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Pretty sweet 4px wide pixel font
 The 'creep' font is a pretty compact font that is only 4 pixels wide. It is
 great for smaller screens, in order to be able to maintain high text density
 on screens reported as small as 11 inches in diagonal.
 .
 Box drawing
 .
 Creep has most of the basic box drawing characters implemented. Therefore
 creep usually works with most ncurses-type programs or with tmux
 window-splitting for example.
 .
 Powerline
 .
 Creep supports all the symbols needed for Lokaltog's awesome powerline plugin
 for vim.
 .
 Sparklines
 .
 Creep has the necessary symbols for creating sparklines. This is cool for
 tools like rainbarf and others.
 .
 Better Haskell syntax
 .
 This font contains characters that can be used to pretty-print Haskell
 symbols, like '>>='.
 .
 Braille and Drawille
 .
 Creep now supports the full braille alphabet, which was an easy thing to do
 because of the clever braille encoding scheme. All of the braille characters
 are simply generated using a little script.
